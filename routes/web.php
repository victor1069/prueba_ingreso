<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','InicioController@index')->name('inicio');
Route::get('/prueba2','InicioController@prueba2')->name('prueba2');
Route::post('/obtenerCoincidencias','InicioController@obtenerCoincidencias')->name('prueba2'); 
