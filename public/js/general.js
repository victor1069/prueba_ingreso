$(document).ready(function(){
	validarFormilarioNewsletter();

	Array.prototype.shuffle = function() {
	    for ( var i = this.length-1; i > 0; i-- ) {
	        var j = Math.floor( i * Math.random() );
	        var tmp = this[ j ];
	        this[ j ] = this[ i ];
	        this[ i ] = tmp;
	    }
	    return this;
	}

	let arrayImgInstagram= imagenes_instagram.shuffle();
	for (var i = arrayImgInstagram.length - 1; i >= 0; i--) {
		let html=`<div class="col-6 col-lg-4 p-0">
                    <img src="${arrayImgInstagram[i]}" class="w-100">
                 </div>`;
        $("#lista_imagenes_instagram").append(html);
	}

	$("#form-parabras").submit(function(e){
		e.preventDefault();
		cargarCoincidencias();
	});
});

function shuffle( myArr ){
  return myArr.sort( function(){ Math.random() - 0.5 });
}

function validarFormilarioNewsletter(){
	$.validator.setDefaults({
		submitHandler: function() {
			let longitudEmail= $("#email").val().length; 
			var tipo=(longitudEmail%2)?"Longitud de Email es Impar":"Longitud de Email es Par"; 

			Swal.fire({
			  title: tipo,
			  icon: 'success',
			  showClass: {
			    popup: 'animated fadeInDown faster'
			  },
			  hideClass: {
			    popup: 'animated fadeOutUp faster'
			  }
			});
		}
	});

	$("#form-newsletter").validate({
			errorLabelContainer: $("#form-newsletter div.error"),
			rules: {
				email: {
					required: true,
					email: true
				}
			},
			messages: {
				email: {
						email:"Ingresa Un Email Valido",
						required: "Email es requerido"
					}
			}
	});
}

function cargarCoincidencias(){
	$("#lista_coincidencias").html('');
	let form= $("#form-parabras").serialize();
	$.ajax({
        type: "POST",
        url: '/obtenerCoincidencias',
        dataType: 'json',
        data: form,
    }).done( function(res) {
        console.log(res);
        $.each(res, function(k,v){
        	let html= `<label class="col-lg-3 col-6 mb-2 p-2">${v}</label>`;
        	$("#lista_coincidencias").append(html);
        });
    }).fail( function(res) {
        console.log(res);
    });
}