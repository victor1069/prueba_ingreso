<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CoincidenciasParabrasModel;

class InicioController extends Controller
{
    public function index(){
    	$imagenes_instagram=array(
    		asset('img/general/Background_4.png'),
    		asset('img/general/Background_3.png'),
    		asset('img/general/Background_2.png'),
    		asset('img/general/Background_1.jpg'),
    		asset('img/general/Background_6.png'),
    		asset('img/general/Background_5.png')
    	);
    	return view('welcome',['imagenes_instagram'=>$imagenes_instagram]);
    }

    public function prueba2(){
    	return view('prueba2',['imagenes_instagram'=>[]]);
    }

    public function obtenerCoincidencias(Request $request)
    {	
    	$objCoincidenciasParabrasModel= new CoincidenciasParabrasModel();
    	return json_encode($objCoincidenciasParabrasModel->coincidencias($request));
    }
}
