<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class CoincidenciasParabrasModel extends Model
{
    public function coincidencias(Request $request){
    	$data = config('listadoParabras');
		$listadoParabras = $data;
		$varPost= $request->all();
		$listaLetras= $this->ajustarString($varPost['lista_letras']);
		$longitud= (int) $varPost['cantidad'];
		$listadeCoincidencias= $this->obtenerString($listadoParabras, $listaLetras, $longitud);
		 // dd($listadeCoincidencias);

    	return $listadeCoincidencias;
    }

    public function obtenerString($listadoParabras=array(), $permitidos = "ab", $longitud=2){
    	$resPalabras=array();
    	for ($l=0; $l < count($listadoParabras); $l++) { 
	      $res= 0;
	      $string= strtolower(trim($listadoParabras[$l]));
	      $logString= (int) strlen(utf8_decode($string));
	      if($logString == $longitud){
	      	$res= 1; 
		      for ($i=0; $i<strlen($string); $i++){
		        if (strpos(strtolower($permitidos), substr($string,$i,1))===false){
		          $res= 0;
		        }
		      }
		  }

	      if($res==1){
	      	$resPalabras[]= trim($listadoParabras[$l]); 
	      }
    	}
      return $resPalabras;
    }

    public function ajustarString($string=''){
      $stringrtn= $string;
      $validarA=0;
      $validarE=0;
      $validarI=0;
      $validarO=0;
      $validarU=0;
      for ($i=0; $i<strlen($string); $i++){
        if (strpos('aA', substr($string,$i,1))===false){
          if($validarA==0){$stringrtn.='áÁ'; $validarA=1;}
        }

        if (strpos('eE', substr($string,$i,1))===false){
          if($validarE==0){$stringrtn.='éÉ'; $validarE=1;}
        }

        if (strpos('iI', substr($string,$i,1))===false){
          if($validarI==0){$stringrtn.='íÍ'; $validarI=1;}
        }

        if (strpos('oO', substr($string,$i,1))===false){
          if($validarO==0){$stringrtn.='óÓ'; $validarO=1;}
        }

        if (strpos('uU', substr($string,$i,1))===false){
          if($validarU==0){$stringrtn.='úÚ'; $validarU=1;}
        }
      }
      return $stringrtn;
    }
}
