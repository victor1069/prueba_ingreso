<section id="carousel">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img src="{{asset('img/carousel/carousel_2.jpg')}}" class="d-block w-100" alt="...">
          <div class="carousel-caption row m-0 align-items-center">
            <div class="d-none d-md-block w-100">
                <label class="h1 text-center">ESTÁR CÓMODO,<br>NUNCA FUE TAN FÁCIL</label>
            </div>
            <div class="btn-carousel w-100 row m-0">
                <button class="btn ml-auto mr-auto border text-white">
                    SHOP
                </button>
            </div>
          </div>
        </div>
        <div class="carousel-item">
          <img src="{{asset('img/carousel/carousel_3.jpg')}}" class="d-block w-100" alt="...">
          <div class="carousel-caption row m-0 align-items-center">
            <div class="btn-carousel w-100 row m-0">
                <button class="btn ml-auto mr-auto border text-white">
                    SHOP
                </button>
            </div>
          </div>
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
</section>