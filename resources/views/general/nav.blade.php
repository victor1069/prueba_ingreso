<div class="bg-nav"></div>
<nav class="navbar navbar-expand-lg navbar-light bg-transparente pl-lg-4 pl-2 pr-lg-4 pr-2 pt-4">
  <a class="navbar-brand" href="/"><img src="{{asset('img/general/LOGO.png')}}"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <div class="navbar-css row m-0 mt-4">
        <ul class="navbar-nav mr-auto ml-auto">
          <li class="nav-item border-r-1">
            <a class="nav-link text-white text-center pl-3 pr-3" href="#"><img src="{{asset('img/general/Shape1.png')}}"><br><span class="font-weight-bold">PUFI PUFF</span></a>
          </li>
          <li class="nav-item border-r-1">
            <a class="nav-link text-white text-center pl-3 pr-3" href="#"><img src="{{asset('img/general/umbrella.png')}}"><br><span class="font-weight-bold">PUFI RAIN</span></a>
          </li>
          <li class="nav-item border-r-1">
            <a class="nav-link text-white text-center pl-3 pr-3" href="#"><img src="{{asset('img/general/Shape2.png')}}"><br><span class="font-weight-bold">PUFI CART</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-white text-center pl-3 pr-3" href="#"><img src="{{asset('img/general/Shape3.png')}}"><br><span class="font-weight-bold">PUFI NAP</span></a>
          </li>
        </ul>
    </div>
    
    <ul class="navbar-nav ml-auto">            
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-white border-r-1" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          MI CUENTA
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="/prueba2">prueba2</a>
          <a class="dropdown-item" href="/">Inicio</a>
        </div>
      </li>
      <li class="nav-item">
            <a class="nav-link text-white" href="#">MI COMPRA</a>
      </li>
    </ul>
  </div>
</nav>