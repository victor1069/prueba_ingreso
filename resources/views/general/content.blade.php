<section class="w-100 row m-0 mb-4">
    <div class="col-12 col-lg-6 p-0 order-1 order-lg-1">
        <div class="btn-card-promo row m-0 align-items-center">
            <button class="btn ml-auto mr-auto border text-white ml-auto mr-auto pl-4 pr-4">
                SHOP
            </button>
        </div>
        <div class="posicion-triandolo right-0 row m-0 align-items-center"><div class="triangulo-1 rotar180 d-none d-lg-block"></div></div>
        <div class="posicion-triandolo-2 bottom-0 row m-0"><div class="triangulo-1 rotar270 d-block d-lg-none ml-auto mr-auto"></div></div>
        <img src="{{asset('img/general/Layer248.jpg')}}" class="w-100">
    </div>
    <div class="col-12 col-lg-6 p-0 order-2 order-lg-2 row m-0 align-items-center bg-color-1">
        <div class="w-100 row m-0 pt-3 pb-3">
            <div class="w-100 text-center"><img src="{{asset('img/general/sombrilla2.png')}}"></div>
            <label class="w-100 text-center h3">Pufi RAIN</label>
            <div class="w-100 row m-0 mb-3"><label class="borde-mini-promo ml-auto mr-auto"></label></div>
            <div class="w-100 text-center mb-5">
                <p class="small">Descripción del producto. Este es <br>
                un texto simulado</p class="small">
            </div>
            <div class="w-100 text-center">
                <button class="btn"> > VER MÁS</button>
            </div>
        </div>
    </div>
    <!-- ##########--########## -->
    <div class="col-12 col-lg-6 p-0 order-4 order-lg-3 row m-0 align-items-center">
        <div class="w-100 row m-0 pt-3 pb-3">
            <div class="w-100 text-center"><img src="{{asset('img/general/Layer230.jpg')}}"></div>
            <label class="w-100 text-center h3">Pufi PUFF</label>
            <div class="w-100 row m-0 mb-3"><label class="borde-mini-promo ml-auto mr-auto"></label></div>
            <div class="w-100 text-center mb-5">
                <p class="small">Descripción del producto. Este es <br>
                un texto simulado</p class="small">
            </div>
            <div class="w-100 text-center">
                <button class="btn"> > VER MÁS</button>
            </div>
        </div>
    </div>
    <div class="col-12 col-lg-6 p-0 order-3 order-lg-4">
        <div class="btn-card-promo row m-0 align-items-center">
            <button class="btn ml-auto mr-auto border text-white ml-auto mr-auto pl-4 pr-4">
                SHOP
            </button>
        </div>
        <div class="posicion-triandolo left-0 row m-0 align-items-center"><div class="triangulo-1 d-none d-lg-block"></div></div>
        <div class="posicion-triandolo-2 bottom-0 row m-0"><div class="triangulo-1 rotar270 d-block d-lg-none ml-auto mr-auto"></div></div>
        <img src="{{asset('img/general/Background.jpg')}}" class="w-100">
    </div>

    <!-- ##########--########## -->
    <div class="col-12 col-lg-6 p-0 order-5 order-lg-5">
        <div class="btn-card-promo row m-0 align-items-center">
            <button class="btn ml-auto mr-auto border text-white ml-auto mr-auto pl-4 pr-4">
                SHOP
            </button>
        </div>
        <div class="posicion-triandolo right-0 row m-0 align-items-center"><div class="triangulo-1 rotar180 d-none d-lg-block"></div></div>
        <div class="posicion-triandolo-2 bottom-0 row m-0"><div class="triangulo-1 rotar270 d-block d-lg-none ml-auto mr-auto"></div></div>
        <img src="{{asset('img/general/Layer240.jpg')}}" class="w-100">
    </div>
    <div class="col-12 col-lg-6 p-0 order-6 order-lg-6 row m-0 align-items-center">
        <div class="w-100 row m-0 pt-3 pb-3">
            <div class="w-100 text-center"><img src="{{asset('img/general/Layer229.jpg')}}"></div>
            <label class="w-100 text-center h3">Pufi CART</label>
            <div class="w-100 row m-0 mb-3"><label class="borde-mini-promo ml-auto mr-auto"></label></div>
            <div class="w-100 text-center mb-5">
                <p class="small">Descripción del producto. Este es <br>
                un texto simulado</p class="small">
            </div>
            <div class="w-100 text-center">
                <button class="btn"> > VER MÁS</button>
            </div>
        </div>
    </div>
    <!-- ##########--########## -->
    <div class="col-12 col-lg-6 p-0 order-8 order-lg-7 row m-0 align-items-center">
        <div class="w-100 row m-0 pt-3 pb-3">
            <div class="w-100 text-center"><img src="{{asset('img/general/Layer228.png')}}"></div>
            <label class="w-100 text-center h3">Pufi NAP</label>
            <div class="w-100 row m-0 mb-3"><label class="borde-mini-promo ml-auto mr-auto"></label></div>
            <div class="w-100 text-center mb-5">
                <p class="small">Descripción del producto. Este es <br>
                un texto simulado</p class="small">
            </div>
            <div class="w-100 text-center">
                <button class="btn"> > VER MÁS</button>
            </div>
        </div>
    </div>
    <div class="col-12 col-lg-6 p-0 order-7 order-lg-8">
        <div class="btn-card-promo row m-0 align-items-center">
            <button class="btn ml-auto mr-auto border text-white ml-auto mr-auto pl-4 pr-4">
                SHOP
            </button>
        </div>
        <div class="posicion-triandolo left-0 row m-0 align-items-center"><div class="triangulo-1 d-none d-lg-block"></div></div>
        <div class="posicion-triandolo-2 bottom-0 row m-0"><div class="triangulo-1 rotar270 d-block d-lg-none ml-auto mr-auto"></div></div>
        <img src="{{asset('img/general/Background2.jpg')}}" class="w-100">
    </div>
</section>

<section class="col-12 col-lg-5 ml-auto mr-auto row m-0">
    <label class="w-100 text-center text-colo-2 pt-3"><small>INSTAGRAM</small></label>
    <label class="w-100 text-center h3 mb-4">#ESPUFI</label>
    <div class="col-12 row m-0 pt-3 pb-3" id="lista_imagenes_instagram"></div>

    <label class="w-100 text-center text-colo-2 pt-3"><small>NEWSLETTER</small></label>
    <label class="w-100 text-center h3">SUSCRIBETE</label>
    <label class="w-100 text-center"><small>Y enterate de todas las novedades</small></label>
    <form id="form-newsletter" class="w-100 row m-0 mb-4">
        <div class="input-group">
            <input type="email" id="email" name="email" class="form-control border-right-0" placeholder="Ingresa tu email" aria-label="Input group example" aria-describedby="btnGroupAddon" required>
            <div class="input-group-prepend">
              <div class="input-group-text bg-white border-left-0 pt-1" id="btnGroupAddon">
                  <button type="submit" class="btn p-0"><img src="{{asset('img/general/flecha.png')}}"></button>
              </div>
            </div>
        </div>
        <div class="w-100 error pt-2 pb-2"></div>
    </form>
</section>