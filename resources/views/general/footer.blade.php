<footer class="w-100 mt-4 row m-0">
           <div class="linea-footer"></div> 

           <div class="col-12 col-lg-11 ml-auto mr-auto p-0 border-bottom row m-0">
              <div class="col-12 col-lg-2 pl-lg-0 pl-3 pr-lg-0 pr-0 mb-3">
                  <img src="{{asset('img/general/logo2.png')}}" class="">
              </div> 
              <div class="col-12 col-lg-10 pb-4 p-0 row m-0">
                  <div class="col-6 col-lg-3 border-r-1 row m-0 mb-3">
                      <label class="w-100 text-center text-lg-left"><a href="#" class="text-secondary">PUFI RAIN</a></label>
                      <label class="w-100 text-center text-lg-left"><a href="#" class="text-secondary">PUFI PUFF</a></label>
                      <label class="w-100 text-center text-lg-left"><a href="#" class="text-secondary">PUFI CART</a></label>
                      <label class="w-100 text-center text-lg-left"><a href="#" class="text-secondary">PUFI NAP</a></label>
                  </div>

                  <div class="col-6 col-lg-3 border-r-1 row m-0 mb-3">
                      <label class="w-100 text-center text-lg-left"><a href="#" class="text-secondary">CONTACTO</a></label>
                      <label class="w-100 text-center text-lg-left"><a href="#" class="text-secondary">AYUDA</a></label>
                      <label class="w-100 text-center text-lg-left"><a href="#" class="text-secondary">CÓMO COMPRAR</a></label>
                      <label class="w-100 text-center text-lg-left"><a href="#" class="text-secondary">TÉRMINOS & CONDICIONES</a></label>
                  </div>

                  <div class="col-12 col-lg-3 border-r-1 row m-0 justify-content-center">
                    <div class="row m-0">
                        <label class="col-lg-auto col-12"><a href="#" class="text-secondary">COMPRA 100% SEGURA</a></label>
                        <div class="w-auto row m-0 mb-3">
                            <img src="{{asset('img/general/Datafiscal.png')}}" class="img-logo-fotter-2 ml-2">
                            <img src="{{asset('img/general/Escudo.png')}}" class="img-logo-fotter-2 ml-2">
                            <span class="small pl-2 lh-fotter-1">COMPRÁ CON<br> LA GARANTÍA <br>DE PUP</span>
                        </div>
                    </div>
                      
                  </div>

                  <div class="col-12 col-lg-3 row m-0">
                      <label class="w-100 row m-0">
                        <span class="ml-lg-2 ml-auto ">SIGUENOS EN </span>
                        <img src="{{asset('img/general/Facebook.png')}}" class="h-icon-redes ml-2">
                        <img src="{{asset('img/general/Twitter.png')}}" class="h-icon-redes ml-2">
                        <img src="{{asset('img/general/Instagram.png')}}" class="h-icon-redes ml-2">
                      </label>
                  </div>
              </div>
           </div>

            <div class="col-12 col-lg-11 ml-auto mr-auto p-0 row m-0">
                <label class="mr-auto text-colo-2 h6"><small>PUFI Copyright 2020 - Todos los derechos reservados</small></label>
                <img src="{{asset('img/general/VectorSmartObject.png')}}" class="w-auto img-logo-fotter">
            </div>
        </footer>