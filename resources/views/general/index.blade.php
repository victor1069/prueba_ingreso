<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@lang('general.prueba')</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('librerias/bootstrap/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('librerias/OwlCarousel/dist/assets/owl.carousel.min.css')}}">
        <!-- <link rel="stylesheet" href="{{asset('librerias/sweetalert2/sweetalert2.css')}}"> -->
        <link rel="stylesheet" href="{{asset('css/general.css')}}">     
    </head>
    <body>
        @yield('nav')
        @yield('header')
        @yield('content')
        @yield('footer')
        <script type="text/javascript">
            var imagenes_instagram = @json($imagenes_instagram);
        </script>
        <script src="{{asset('librerias/jquery/jquery-3.4.1.min.js')}}"></script>
        <script src="{{asset('librerias/jquery/jquery.validate.min.js')}}"></script>
        <script src="{{asset('librerias/bootstrap/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('librerias/OwlCarousel/dist/owl.carousel.min.js')}}"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script src="{{asset('js/general.js')}}"></script>
    </body>
</html>
<!-- 
CONSTRUCCION VICTOR MANUEL GARCIA
TIEMPO 7 HORAS -->