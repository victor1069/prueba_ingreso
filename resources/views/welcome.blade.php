@extends('general.index')

@section('nav')
    @include('general.nav')
@endsection

@section('header')
    @include('general.header')
@endsection

@section('content')
    @include('general.content')
@endsection

@section('footer')
    @include('general.footer')
@endsection