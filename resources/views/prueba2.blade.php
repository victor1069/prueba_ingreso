@extends('general.index')

@section('nav')
    @include('general.nav')
@endsection

@section('header')
@endsection

@section('content')
    <div class="h-min-nav"></div>
    <div class="col-12 row m-0 mb-3">
    	<div class="col-lg-7 col-12 ml-auto mr-auto">
    		<label class="h1 text-center">Listar palabras de un conjunto de letras</label>
    		<form class="col-12 row m-0" id="form-parabras">
    			@csrf
    			<div class="col-12 col-lg-8 mb-2 p-1">
    				<input type="text" class="form-control" name="lista_letras" placeholder="Lista De Letras" required="required" autocomplete="off">
    			</div>

    			<div class="col-6 col-lg-2 mb-2 p-1">
    				<input type="number" class="form-control" name="cantidad" placeholder="Cantidad De Letras" required="required" autocomplete="off">
    			</div>

    			<div class="col-6 col-lg-2 mb-2 p-1">
    				<button type="submit" class="btn btn-primary">GO</button>
    			</div>
    		</form>
    	</div>

    	<div class="col-lg-7 col-12 ml-auto mr-auto mb-3">
    		<label class="h3 text-center mb-3 font-weight-bold">Coincidencias</label>

    		<div class="col-12 border row m-0" id="lista_coincidencias">
    		</div>
    	</div>

    </div>
@endsection

@section('footer')
    @include('general.footer')
@endsection